package eu.pb.payment_tracker;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;

/**
 * Payment record per currency containing currency, amount and exchange rate data
 */
public class PaymentRecord {

    private static final int AMOUNT_SCALE = 2;
    private static final int CURRENCY_RIGHT_PAD = 3;

    private String currency;

    private BigDecimal amount;

    private BigDecimal exchangeRate;

    public PaymentRecord(String currency) {
        this.currency = currency;
        amount = BigDecimal.ZERO;
        exchangeRate = BigDecimal.ONE;
    }

    /**
     * Add {@code amount} to the current amount
     *
     * @param amount desired amount to add
     */
    public void addAmount(BigDecimal amount) {
        requireNonNull(amount, "Amount can not be null");
        this.amount = this.amount.add(amount);
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentRecord that = (PaymentRecord) o;

        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        return exchangeRate != null ? exchangeRate.equals(that.exchangeRate) : that.exchangeRate == null;
    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (exchangeRate != null ? exchangeRate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        String paddedCurrency = StringUtils.rightPad(currency, CURRENCY_RIGHT_PAD, " ");
        String formattedAmount = amount.setScale(AMOUNT_SCALE,
                BigDecimal.ROUND_HALF_UP)
                .toPlainString();

        if (PaymentTracker.CURRENCY_USD.equals(currency)) {
            // Display no exchange rate for USD
            return String.format("%s %s", paddedCurrency, formattedAmount);
        } else {
            return String.format("%s %s (USD %s)",
                    paddedCurrency,
                    formattedAmount,
                    amount.multiply(exchangeRate)
                            .setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_UP)
                            .toPlainString()
            );
        }
    }
}
