package eu.pb.payment_tracker.exception;

/**
 * RT exception used to wrap checked exception when desired
 */
public class PaymentTrackerException extends RuntimeException {
    /**
     *
     * @param message exception message
     * @param cause exception root cause
     */
    public PaymentTrackerException(String message, Throwable cause) {
        super(message, cause);
    }
}
