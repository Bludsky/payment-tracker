package eu.pb.payment_tracker;

import asg.cliche.Command;

import java.math.BigDecimal;

/**
 * Shell command parser reading and parsing console input
 */
public class ShellParser {

    private PaymentTracker tracker;

    public ShellParser(PaymentTracker tracker) {
        this.tracker = tracker;
    }

    @Command
    public void add(String currency, double amount) {
        tracker.addPayment(currency, BigDecimal.valueOf(amount));
    }

    @Command
    public void rate(String currency, double exchangeRate) {
        tracker.changeRate(currency, BigDecimal.valueOf(exchangeRate));
    }

    @Command
    public void print() {
        tracker.print();
    }

    @Command
    public void help() {
        System.out.println("Available commands:");
        System.out.println("add <currency> <amount>");
        System.out.println("rate <currency> <exchange rate>");
        System.out.println("print");
        System.out.println("quit");
    }

    @Command
    public void quit() {
        System.exit(0);
    }

}
