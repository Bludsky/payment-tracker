package eu.pb.payment_tracker;

import eu.pb.payment_tracker.exception.PaymentTrackerException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;

import static eu.pb.payment_tracker.PaymentTrackerUtils.isValidCurrency;
import static eu.pb.payment_tracker.PaymentTrackerUtils.parseTuple;
import static java.util.Objects.requireNonNull;

/**
 * Default {@link PaymentTracker} implementation
 */
public class PaymentTrackerImpl implements PaymentTracker {

    private static final Logger log = LoggerFactory.getLogger(PaymentTrackerImpl.class);

    private ConcurrentMap<String, PaymentRecord> payments = new ConcurrentHashMap<>();

    private static final long PRINT_PERIOD_SECONDS = 60;

    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public PaymentTrackerImpl() {
        // Periodically print stored content
        executorService.scheduleAtFixedRate(() -> print(),
                PRINT_PERIOD_SECONDS,
                PRINT_PERIOD_SECONDS,
                TimeUnit.SECONDS);
    }

    @Override
    public void addPayment(String currency, BigDecimal amount) {
        requireNonNull(currency, "Currency can not be null when adding payment");
        requireNonNull(amount, "Amount can not be null when adding payment");

        log.info("Adding payment {} {}", currency, amount);

        if (!isValidCurrency(currency)) {
            log.warn("Payment could not be addded due to invalid currency format");
            return;
        }

        initializeCurrency(currency);
        payments.get(currency).addAmount(amount);
    }

    @Override
    public void changeRate(String currency, BigDecimal exchangeRate) {
        requireNonNull(currency, "Currency can not be null when changing rate");
        requireNonNull(exchangeRate, "Exchange rate can not be null when changing rate");

        log.info("Changing exchange rate for {} to {}", currency, exchangeRate);

        if (!isValidCurrency(currency)) {
            log.warn("Exchange rate could not be set due to invalid currency format");
            return;
        }

        if (exchangeRate.compareTo(BigDecimal.ZERO) <= 0) {
            log.warn("Only positive rates allowed. No change will occur.");
            return;
        }

        initializeCurrency(currency);
        payments.get(currency).setExchangeRate(exchangeRate);
    }

    @Override
    public void loadPayments(InputStream is) {
        log.info("Reading payments from stream");
        readLines(is).stream()
                .forEach(line -> {
                    Optional<Pair<String, BigDecimal>> pair = parseTuple(line);
                    if (pair.isPresent()) {
                        addPayment(pair.get().getLeft(), pair.get().getRight());
                    }
                });
    }

    @Override
    public void loadExchangeRates(InputStream is) {
        log.info("Reading exchange rates from stream");
        readLines(is).stream()
                .forEach(line -> {
                    Optional<Pair<String, BigDecimal>> pair = parseTuple(line);
                    if (pair.isPresent()) {
                        changeRate(pair.get().getLeft(), pair.get().getRight());
                    }
                });
    }

    // Apache IOUtils#readLines causing unsafe warning as it returns generic collection
    @SuppressWarnings("unchecked")
    private List<String> readLines(InputStream is) {
        requireNonNull(is, "Input stream can not be null");

        try {
            return IOUtils.readLines(is, StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            String errMsg = "Error reading data from input stream";
            log.error(errMsg, e);
            throw new PaymentTrackerException(errMsg, e);
        }
    }

    @Override
    public void print() {
        System.out.println("Displaying content:");
        payments.keySet()
                .stream()
                .filter(key -> BigDecimal.ZERO.compareTo(payments.get(key).getAmount()) != 0)
                .sorted()
                .forEachOrdered(key -> System.out.println(payments.get(key)));
    }

    @Override
    public Optional<PaymentRecord> findPayment(String currency) {
        requireNonNull(currency, "Currency can not be null");
        return Optional.ofNullable(payments.get(currency));
    }

    private void initializeCurrency(String currency) {
        if (payments.putIfAbsent(currency, new PaymentRecord(currency)) == null) {
            log.info("Currency {} initialized", currency);
        }
    }

}
