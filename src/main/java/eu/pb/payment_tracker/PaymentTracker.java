package eu.pb.payment_tracker;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * Payment Tracker component
 */
public interface PaymentTracker {

    String CURRENCY_USD = "USD";

    /**
     * Add {@code amount} of payment to {@code currency}
     *
     * @param currency currency {@code amount} will be added to
     * @param amount amount for designated {@code currency}
     */
    void addPayment(String currency, BigDecimal amount);

    /**
     * Set exchange rate of {@code currency}
     *
     * @param currency currency rate will be se to
     * @param exchangeRate exchange rate compared to USD
     */
    void changeRate(String currency, BigDecimal exchangeRate);

    /**
     * Read lines from input stream {@code is} and parse its data.
     * Each line should use the following format: CURRENCY AMOUNT (e.g. USD 50)
     *
     * @param is input stream containing formatted line data
     */
    void loadPayments(InputStream is);

    /**
     * Read exchange rates from input stream {@code is} and parse its data.
     * Each line should use the following format: CURRENCY EXCHANGE_RATE (e.g. CZK 0.042)
     *
     * @param is input stream containing formatted line data
     */
    void loadExchangeRates(InputStream is);

    /**
     * Print all stored currencies and their amounts
     */
    void print();

    /**
     * Find {@link PaymentRecord} by {@code currency}
     *
     * @param currency
     * @return
     */
    Optional<PaymentRecord> findPayment(String currency);
}
