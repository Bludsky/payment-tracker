package eu.pb.payment_tracker;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Payment Tracker utility methods
 */
public final class PaymentTrackerUtils {

    private static final Logger log = LoggerFactory.getLogger(PaymentTrackerUtils.class);

    private static final Pattern PATTERN_LINE = Pattern.compile("([A-Z]{3})\\s([-]?\\d+(\\.\\d+)?)");
    private static final Pattern PATTERN_CURRENCY = Pattern.compile("[A-Z]{3}");

    private PaymentTrackerUtils() {
    }

    /**
     * Parse tuple<String, BigDecimal> from {@code line}
     *
     * @param line line containing data matching PATTERN_LINE
     * @return tuple containing extracted data
     */
    static Optional<Pair<String, BigDecimal>> parseTuple(String line) {
        Matcher matcher = PATTERN_LINE.matcher(line);
        if (matcher.matches()) {
            String currency = matcher.group(1);
            BigDecimal value = new BigDecimal(matcher.group(2));
            return Optional.of(Pair.of(currency, value));
        } else {
            log.warn("Ignoring line {}. No relevant data match found.", line);
        }
        return Optional.empty();
    }

    /**
     * Checks whether {@code currency} conforms to specified currency format
     *
     * @param currency currency to be tested
     * @return {@code true} if {@code currency} conforms to valid format otherwise {@code false}
     */
    static boolean isValidCurrency(String currency) {
        Matcher matcher = PATTERN_CURRENCY.matcher(currency);
        if (matcher.matches()) {
            return true;
        } else {
            log.warn("Currency {} does not conform to valid format (3 upper case letters, e.g. USD)", currency);
            return false;
        }
    }

}
