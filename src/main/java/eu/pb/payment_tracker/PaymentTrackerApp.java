package eu.pb.payment_tracker;

import asg.cliche.ShellFactory;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Payment Tracker application entry point
 *
 * For usage please see README.md
 */
public class PaymentTrackerApp {

    public static final String APP_NAME = "Payment Tracker";

    private static final Logger log = LoggerFactory.getLogger(PaymentTrackerApp.class);

    public static void main(String[] args) {
        CommandLineParser parser = new DefaultParser();

        // Options for args parsing
        Options options = prepareOptions();

        // Application usage
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("ant", options);

        // Initialize Payment Tracker instance
        PaymentTracker tracker = new PaymentTrackerImpl();

        try {
            CommandLine commandLine = parser.parse(options, args);

            if (commandLine.hasOption(Parameter.PAYMENT_FILE)) {
                // Payment file specified
                try {
                    String pathToPaymentFile = commandLine.getOptionValue(Parameter.PAYMENT_FILE);
                    log.info("Loading payments from {}", pathToPaymentFile);
                    InputStream is = loadFile(pathToPaymentFile);
                    tracker.loadPayments(is);
                } catch (IOException e) {
                    log.error("Payment file could not be read. The application will continue nonetheless.");
                }
            }

            if (commandLine.hasOption(Parameter.RATE_FILE)) {
                // Rate file specified
                try {
                    String pathToRateFile = commandLine.getOptionValue(Parameter.RATE_FILE);
                    log.info("Loading exchange rates from {}", pathToRateFile);
                    InputStream is = loadFile(pathToRateFile);
                    tracker.loadExchangeRates(is);
                } catch (IOException e) {
                    log.error("Rate file could not be read. The application will continue nonetheless.");
                }
            }

        } catch (ParseException e) {
            log.error("The command line arguments could not be parsed. " +
                    "The application will continue nonetheless.", e);
        }

        try {
            System.out.println("Type \"help\" to see all available commands");
            ShellFactory.createConsoleShell(APP_NAME, APP_NAME, new ShellParser(tracker))
                    .commandLoop();
        } catch (IOException e) {
            String errMsg = "Console shell exception encountered";
            log.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
    }

    private static Options prepareOptions() {
        Options options = new Options();

        Option paymentFileOption = Option.builder()
                .longOpt(Parameter.PAYMENT_FILE)
                .argName("path to payment file")
                .desc("Absolute path to payment file with currencies and amounts stored" +
                        ", e.g. /Users/pb/payment.txt")
                .hasArg(true)
                .numberOfArgs(1)
                .build();

        Option rateFileOption = Option.builder()
                .longOpt(Parameter.RATE_FILE)
                .argName("path to rate file")
                .desc("Absolute path to data file with currencies and exchange rates stored" +
                        ", e.g. /Users/pb/rate.txt")
                .hasArg(true)
                .numberOfArgs(1)
                .build();

        options.addOption(paymentFileOption);
        options.addOption(rateFileOption);

        return options;
    }

    private static InputStream loadFile(String pathToFile) throws IOException {
        Path path = Paths.get(pathToFile);
        return Files.newInputStream(path);
    }

    /**
     * Application parameter names
     */
    interface Parameter {
        String PAYMENT_FILE = "paymentFile";
        String RATE_FILE = "rateFile";
    }

}
