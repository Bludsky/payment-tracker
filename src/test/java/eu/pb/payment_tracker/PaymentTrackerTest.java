package eu.pb.payment_tracker;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Set of unit tests for {@link PaymentTracker}
 */
public class PaymentTrackerTest {

    private PaymentTracker tracker;

    @Before
    public void setUp() throws Exception {
        tracker = new PaymentTrackerImpl();
    }

    @Test
    public void initializePaymentTest() throws Exception {
        final String currency = "USD";

        tracker.addPayment(currency, BigDecimal.ONE);
        Optional<PaymentRecord> payment = tracker.findPayment(currency);

        assertTrue(payment.isPresent());
        assertEquals(BigDecimal.ONE, payment.get().getAmount());
    }

    @Test
    public void addPaymentTest() throws Exception {
        final String currency = "USD";

        tracker.addPayment(currency, BigDecimal.ONE);
        tracker.addPayment(currency, BigDecimal.valueOf(-1));

        Optional<PaymentRecord> payment = tracker.findPayment(currency);
        assertTrue(payment.isPresent());
        assertEquals(BigDecimal.ZERO, payment.get().getAmount());
    }

    @Test
    public void invalidPaymentTest() throws Exception {
        final String currency = "USDX";

        tracker.addPayment(currency, BigDecimal.ZERO);
        Optional<PaymentRecord> payment = tracker.findPayment(currency);
        assertFalse(payment.isPresent());
    }

    @Test
    public void initializeExchangeRateTest() throws Exception {
        final String currency = "USD";

        tracker.changeRate(currency, BigDecimal.TEN);

        Optional<PaymentRecord> payment = tracker.findPayment(currency);
        assertTrue(payment.isPresent());
        assertEquals(BigDecimal.TEN, payment.get().getExchangeRate());
    }

    @Test
    public void exchangeRateTest() throws Exception {
        final String currency = "USD";

        tracker.changeRate(currency, BigDecimal.TEN);
        tracker.changeRate(currency, BigDecimal.ONE);

        Optional<PaymentRecord> payment = tracker.findPayment(currency);
        assertTrue(payment.isPresent());
        assertEquals(BigDecimal.ONE, payment.get().getExchangeRate());
    }

    @Test
    public void invalidExchangeRateTest() throws Exception {
        final String invalidCurrency = "USDX";
        final String currency = "USD";

        tracker.changeRate(invalidCurrency, BigDecimal.ZERO);
        Optional<PaymentRecord> payment = tracker.findPayment(invalidCurrency);
        assertFalse(payment.isPresent());

        tracker.changeRate(currency, BigDecimal.valueOf(0));
        payment = tracker.findPayment(currency);
        assertFalse(payment.isPresent());

        tracker.changeRate(currency, BigDecimal.valueOf(-1));
        payment = tracker.findPayment(currency);
        assertFalse(payment.isPresent());
    }

    @Test
    public void findNoPaymentTest() throws Exception {
        assertEquals(Optional.empty(), tracker.findPayment("NIL"));
    }

    @Test
    public void loadPaymentsTest() throws Exception {
        tracker.loadPayments(loadFile("testPayments.txt"));

        assertEquals(BigDecimal.valueOf(-50),
                tracker.findPayment("USD").get().getAmount());
        assertEquals(BigDecimal.valueOf(322.4),
                tracker.findPayment("CZK").get().getAmount());
        assertEquals(BigDecimal.valueOf(664.3321),
                tracker.findPayment("HKD").get().getAmount());
    }

    @Test
    public void loadExchangeRatesTest() throws Exception {
        tracker.loadExchangeRates(loadFile("testRates.txt"));

        assertEquals(BigDecimal.ONE,
                tracker.findPayment("USD").get().getExchangeRate());
        assertEquals(BigDecimal.valueOf(0.042),
                tracker.findPayment("CZK").get().getExchangeRate());
        assertEquals(BigDecimal.valueOf(0.128),
                tracker.findPayment("HKD").get().getExchangeRate());
    }

    /**
     * No asserts, confirm the console formatting only
     */
    @Test
    public void printTest() throws Exception {
        tracker.addPayment("USD", BigDecimal.TEN);
        tracker.addPayment("HKD", BigDecimal.valueOf(44.5322));
        tracker.addPayment("RMB", BigDecimal.valueOf(1566.33));

        tracker.print();
    }

    private InputStream loadFile(String fileName) throws IOException {
        Path path = Paths.get("src/test/resources/test-suite/" + fileName);
        return Files.newInputStream(path);
    }
}
