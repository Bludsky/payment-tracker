# Payment Tracker

## Description
Payment tracking application registering payments (currency, amount and rate) and periodically displaying output.

## Prerequisites
* If you do not have Gradle installed feel free to use the included wrapper. Just replace *gradle* command below with *./gradlew* or *./gradlew.bat* depending on your OS. 

## Usage
1. *gradle run -q* or with optional arguments *gradle run -q -Dexec.args="--paymentFile \<path to payment file\> --rateFile \<path to exchange rate file\>"*
2. Wait a brief moment for dependencies & build
3. Interact with shell & enjoy

## Optional argument list
* --paymentFile \<path to data file with currencies and amount stored\>
* --rateFile \<path to exchange rate file\>

## Command list
* add \<currency\> \<amount\> (e.g. *add USD 50*)
* rate \<currency\> \<exchange rate\> (e.g. *rate CZK 0.042*)
* print
* quit
 
## Error handling
* Invalid data file paths are detected and logged
* Malformed lines from data files are ignored and logged
* Invalid commands are detected and indicated to the user
* Invalid user input is detected and indicated to the user
* Application exits only upon encountering irrecoverable error or upon detecting the *quit* command

## Misc
* Default amount for newly initialized currency is 0, exchange rate to USD is 1
* Amounts are rounded up using scale of 2
* To clean & build & test run *gradle clean build*
* To invoke unit tests run *gradle test*
* Refer to *test-suite/testPayments.txt*, *test-suite/testRates.txt* for example of input files